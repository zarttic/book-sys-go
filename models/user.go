package models

import (
	"time"
)

// User
//
//	@Description:
//	@Author: zarttic
type User struct {
	Id         int       `json:"id"`
	Username   string    `json:"username"`
	Password   string    `json:"password"`
	CreateTime time.Time `json:"create_time"`
	UpdateTime time.Time `json:"update_time"`
}

// TableName
// ////////////////////////////////
//
//	@Description: 关联表名
//	@receiver *User
//	@return string
//
// ////////////////////////////////
func (*User) TableName() string {
	return "user"
}

//func (u *User) BeforeSave(tx *gorm.DB) (err error) {
//	u.UpdateTime = time.Now()
//	return nil
//}
