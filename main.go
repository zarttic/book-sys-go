package main

import (
	"book/dao"
	"book/routers"
)

// main
// ////////////////////////////////
//
//	@Description: 启动
//
// ////////////////////////////////
func main() {
	dao.InitDB()
	dao.InitRD()
	r := routers.InitRouter()

	r.Run(":9421")
}
