package controller

import (
	"book/dao"
	"book/utils"
	"fmt"
	"github.com/gin-gonic/gin"
)

// Login
// ////////////////////////////////
//
//	@Description: 管理员登录
//	@param ctx
//
// ////////////////////////////////
func Login(ctx *gin.Context) {
	dto := struct {
		Name string `json:"username"`
		Pass string `json:"password"`
	}{}
	_ = ctx.BindJSON(&dto)
	fmt.Println(dto)
	admin, cnt := dao.AdminLogin(dto.Name, dto.Pass)
	fmt.Println(cnt)
	if cnt == 0 {
		ctx.JSON(200, gin.H{
			"msg":  "账号或密码错误",
			"data": nil,
			"code": "201",
		})
		return
	}

	ctx.JSON(200, gin.H{
		"msg":  "ok",
		"data": admin,
		"code": "200",
	})

}

// Register
// ////////////////////////////////
//
//	@Description: 管理员注册
//	@param ctx
//	@note 仅限于内部测试使用
//
// ////////////////////////////////
func Register(ctx *gin.Context) {
	name := ctx.PostForm("username")
	pass := ctx.PostForm("password")
	_, cnt := dao.AdminLogin(name, pass)
	if cnt >= 1 {
		ctx.JSON(102, gin.H{
			"msg":  "用户已存在",
			"data": nil,
		})
		return
	}

	if ok := dao.AdminRegister(name, pass); ok >= 1 {
		ctx.JSON(200, gin.H{
			"msg":  "注册成功",
			"data": nil,
		})
	}

}

// ChangePassword
// ////////////////////////////////
//
//	@Description: 修改管理密码
//	@param ctx
//
// ////////////////////////////////
func ChangePassword(ctx *gin.Context) {
	ch := struct {
		Name string `json:"username"`
		Old  string `json:"old"`
		NewP string `json:"new"`
	}{}
	_ = ctx.BindJSON(&ch)
	fmt.Println(ch)
	if len(ch.Old) == 0 || len(ch.NewP) == 0 {
		ctx.JSON(200, gin.H{
			"msg":  "不能为空",
			"data": nil,
			"code": "201",
		})
		return
	}
	admin, cnt := dao.AdminLogin(ch.Name, ch.Old)
	fmt.Println(admin)
	if cnt < 1 || admin.Password != utils.M5Encode(ch.Old) {
		ctx.JSON(200, gin.H{
			"msg":  "旧密码错误",
			"data": nil,
			"code": "201",
		})
		return
	}
	admin.Password = utils.M5Encode(ch.NewP)
	dao.GDB.Save(&admin)
	ctx.JSON(200, gin.H{
		"msg":  "修改成功",
		"data": admin,
		"code": "200",
	})

}
