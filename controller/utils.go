package controller

import (
	"book/dao"
	"github.com/gin-gonic/gin"
)

// Count
// ////////////////////////////////
//
//	@Description: 统计一些系统数据
//	@param ctx
//
// ////////////////////////////////
func Count(ctx *gin.Context) {
	cnt := [3]int64{}
	cnt[0] = dao.UserCount()
	cnt[1] = dao.BookNums()
	cnt[2] = dao.ChapterNums()
	ctx.JSON(200, gin.H{
		"msg":  "ok",
		"data": cnt,
	})
}
