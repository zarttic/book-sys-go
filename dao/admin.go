package dao

import (
	"book/models"
	"book/utils"
	"time"
)

// AdminLogin
// ////////////////////////////////
//
//	@Description: 管理员登录
//	@param name
//	@param password
//	@return admin
//	@return cnt
//
// ////////////////////////////////
func AdminLogin(name, password string) (admin *models.Admin, cnt int64) {
	password = utils.M5Encode(password)
	GDB.Where("username = ?", name).Where("password = ?", password).Find(&admin).Count(&cnt)
	return
}

// AdminRegister
// ////////////////////////////////
//
//	@Description: 管理员注册
//	@param name
//	@param password
//	@return cnt
//	@note 只是用于内部测试不对外开放
//
// ////////////////////////////////
func AdminRegister(name, password string) (cnt int64) {
	password = utils.M5Encode(password)
	ad := models.Admin{
		Username:   name,
		Password:   password,
		Ip:         "112.86.84.146",
		CreateTime: time.Now(),
		UpdateTime: time.Now(),
	}
	GDB.Create(&ad).Count(&cnt)
	return

}
