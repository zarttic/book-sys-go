package controller

import (
	"book/dao"
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
)

// UserCount
// ////////////////////////////////
//
//	@Description: 查询用户的数量
//	@param ctx
//
// ////////////////////////////////
func UserCount(ctx *gin.Context) {

	ctx.JSON(200, gin.H{
		"code": 200,
		"msg":  "成功",
		"data": dao.UserCount(),
	})
}

// UserPages
// ////////////////////////////////
//
//	@Description: 用户分页查询
//	@param ctx
//
// ////////////////////////////////
func UserPages(ctx *gin.Context) {
	No := ctx.Query("pageNo")
	Size := ctx.Query("pageSize")
	name := ctx.Query("name")
	//if len(name) == 0 {
	//	name = " "
	//}
	fmt.Println(name)
	n, _ := strconv.Atoi(No)
	s, _ := strconv.Atoi(Size)
	cnt := dao.UserCount()
	users, _ := dao.UserPage(n, s, name)
	fmt.Println(cnt)
	ctx.JSON(200, gin.H{
		"msg":     "ok",
		"code":    "200",
		"records": users,
		"total":   cnt,
	})
}

// DelUser
// ////////////////////////////////
//
//	@Description: 删除用户
//	@param ctx
//
// ////////////////////////////////
func DelUser(ctx *gin.Context) {
	value := ctx.Query("id")
	dao.DelUser(value)
}

// ChangeUserName
// ////////////////////////////////
//
//	@Description: 修改用户的名字
//	@param ctx
//
// ////////////////////////////////
func ChangeUserName(ctx *gin.Context) {
	id := ctx.Query("id")
	username := ctx.Query("username")
	fmt.Println(id, username)
	cnt := dao.ChangeName(id, username)
	if cnt < 1 {
		ctx.JSON(200, gin.H{
			"code": 201,
			"msg":  "更新失败",
		})
		return
	}
	ctx.JSON(200, gin.H{
		"code": "200",
		"msg":  "更新成功",
	})
}

// RegisterUser
// ////////////////////////////////
//
//	@Description: 注册用户
//	@param ctx
//
// ////////////////////////////////
func RegisterUser(ctx *gin.Context) {
	var tem struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	ctx.BindJSON(&tem)
	if len(tem.Username) == 0 || len(tem.Password) == 0 {
		ctx.JSON(200, gin.H{
			"code": "201",
			"msg":  "参数错误！",
		})
		return
	}

	if dao.FindUserByName(tem.Username) > 0 {
		ctx.JSON(200, gin.H{
			"code": 201,
			"msg":  "用户已存在",
		})
		return
	}
	dao.Register(tem.Username, tem.Password)
	ctx.JSON(200, gin.H{
		"code": 200,
		"msg":  "注册成功",
	})
}
