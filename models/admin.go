package models

import "time"

// Admin
//
//	@Description:
//	@Author: zarttic
type Admin struct {
	Id         int       `json:"id"`
	Username   string    `json:"username"`
	Password   string    `json:"password"`
	Ip         string    `json:"ip"`
	CreateTime time.Time `json:"create_time"`
	UpdateTime time.Time `json:"update_time"`
}

// TableName
// ////////////////////////////////
//
//	@Description: 对应表名
//	@receiver Admin
//	@return string
//
// ////////////////////////////////
func (Admin) TableName() string {
	return "admin"
}
