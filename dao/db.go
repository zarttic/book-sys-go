package dao

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

var GDB *gorm.DB

// InitDB
// ////////////////////////////////
//
//	@Description: 初始化mysql数据库
//
// ////////////////////////////////
func InitDB() {
	dsn := "root:123456@tcp(101.34.56.158:3306)/novel?charset=utf8mb4&parseTime=True&loc=Local"
	GDB, _ = gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
}
