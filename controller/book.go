package controller

import (
	"book/dao"
	"book/models"
	"book/utils"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gomodule/redigo/redis"
	"sort"
	"strconv"
)

// BookNum
// ////////////////////////////////
//
//	@Description: 统计小说的数目
//	@param ctx
//
// ////////////////////////////////
func BookNum(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"code": 200,
		"msg":  "成功",
		"data": dao.BookNums(),
	})
}

// ChapterNum
// ////////////////////////////////
//
//	@Description: 统计所有的章数
//	@param ctx
//
// ////////////////////////////////
func ChapterNum(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"code": 200,
		"msg":  "成功",
		"data": dao.ChapterNums(),
	})
}

// BookPages
//
// ////////////////////////////////
//
//	@Description: bookinfo分页数据
//	@param ctx
//
// ////////////////////////////////
func BookPages(ctx *gin.Context) {
	No := ctx.Query("pageNo")
	Size := ctx.Query("pageSize")
	n, _ := strconv.Atoi(No)
	s, _ := strconv.Atoi(Size)
	var book []models.BookInfo
	reply, _ := redis.Bytes(dao.GRD.Do("get", "book"))
	if len(reply) == 0 {
		book, _ = dao.BookPage(n, s)
		marshal, _ := json.Marshal(book)
		dao.GRD.Do("set", "book", marshal, "EX", "100")
	} else {
		json.Unmarshal(reply, &book)
	}
	//fmt.Println(reply)
	ctx.JSON(200, gin.H{
		"msg":     "ok",
		"code":    "200",
		"records": book,
		"total":   cnt,
	})
}

type static struct {
	Name string
	Per  float64
	Cnt  int64
}
type DT struct {
	Cnt int     `json:"cnt"`
	Per float32 `json:"per"`
}

// BookStatic
// ////////////////////////////////
//
//	@Description: 书籍的一些数据
//	@param ctx
//
// ////////////////////////////////
func BookStatic(ctx *gin.Context) {
	info, cnt := dao.BookStatic()
	m := make(map[string]int64)
	for i := range info {
		m[info[i].Kind]++
	}
	fmt.Println(m)
	fmt.Println(cnt)
	//res := make(map[string]float32)
	var fin []static
	for k, v := range m {
		//res[k] =
		fin = append(fin, static{
			Name: k,
			Per:  utils.Decimal(float64(float32(v) / float32(cnt) * 100)),
			Cnt:  v,
		})
	}
	//排个序
	sort.SliceStable(fin, func(i, j int) bool {
		return fin[i].Cnt > fin[j].Cnt
	})
	ctx.JSON(200, gin.H{
		"msg":  "成功",
		"code": "200",
		"data": fin,
	})
}

// BookAll
// ////////////////////////////////
//
//	@Description: 所有图书的数据
//	@param ctx
//
// ////////////////////////////////
func BookAll(ctx *gin.Context) {
	info, _ := dao.BookStatic()
	ctx.JSON(200, gin.H{
		"msg":  "成功",
		"code": "200",
		"data": info,
	})
}

type BookRank struct {
	Name  string
	Count int
}

// ReadRank
// ////////////////////////////////
//
//	@Description: 图书阅读量
//	@param ctx
//	@note:传出 book_name ,click_count
//
// ////////////////////////////////
func ReadRank(ctx *gin.Context) {
	info, _ := dao.BookStatic()

	var bookrank []BookRank
	for _, bookInfo := range info {
		bookrank = append(bookrank, BookRank{Name: bookInfo.Name, Count: bookInfo.ClickCount})
	}
	sort.SliceStable(bookrank, func(i, j int) bool {
		return bookrank[i].Count < bookrank[j].Count
	})
	ctx.JSON(200, gin.H{
		"data": bookrank,
		"msg":  "ok",
		"code": 200,
	})
}

// ChangeIntro
// ////////////////////////////////
//
//	@Description: 修改图书简介
//	@param ctx
//
// ////////////////////////////////
func ChangeIntro(ctx *gin.Context) {

	var tem struct {
		Id    string `json:"id"`
		Intro string `json:"intro"`
	}
	ctx.BindJSON(&tem)
	fmt.Println(tem.Id, tem.Intro)
	fmt.Println(tem.Intro)
	dao.ChangeIntroByID(tem.Id, tem.Intro)

	reply, _ := redis.Bytes(dao.GRD.Do("get", "book"))
	if len(reply) != 0 {
		dao.GRD.Do("del", "book")
	}
	book, _ := dao.BookPage(1, 100)
	marshal, _ := json.Marshal(book)
	dao.GRD.Do("set", "book", marshal, "EX", "100")
	ctx.JSON(200, gin.H{
		"code": 200,
	})
}
