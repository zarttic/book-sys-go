package dao

import (
	"book/models"
	"book/utils"
	"time"
)

// UserCount
// ////////////////////////////////
//
//	@Description: 统计user的数量
//	@return ans
//
// ////////////////////////////////
func UserCount() (ans int64) {
	GDB.Model(&models.User{}).Count(&ans)
	return
}

// UserPage
// ////////////////////////////////
//
//	@Description: 分页查询user
//	@param no
//	@param size
//	@param name
//	@return users
//	@return cnt
//
// ////////////////////////////////
func UserPage(no, size int, name string) (users []models.User, cnt int64) {
	GDB.Model(&models.User{}).Scopes(utils.Paginate(no, size)).Where("username like ?", "%"+name+"%").Find(&users).Count(&cnt)
	return
}

// DelUser
// ////////////////////////////////
//
//	@Description: 删除用户
//	@param id
//
// ////////////////////////////////
func DelUser(id string) {
	GDB.Delete(&models.User{}, id)
}

// ChangeName
// ////////////////////////////////
//
//	@Description: 修改用户信息
//	@param id
//	@param username
//	@return cnt
//
// ////////////////////////////////
func ChangeName(id, username string) (cnt int64) {
	user := models.User{}
	GDB.Where("id = ?", id).Find(&user)
	user.Username = username
	user.UpdateTime = time.Now()
	GDB.Save(user).Count(&cnt)
	return
}

func FindUserByName(username string) (cnt int64) {
	var user models.User
	GDB.Find(&user).Where("username = ?", username).Count(&cnt)
	return
}

func Register(username, password string) {

	GDB.Save(&models.User{
		Username:   username,
		Password:   password,
		CreateTime: time.Now(),
		UpdateTime: time.Now(),
	})

}
