package models

// BookChapter
//
//	@Description:
//	@Author: zarttic
type BookChapter struct {
	Id      int    `json:"id"`
	BookId  int    `json:"book_id"`
	Name    string `json:"name"`
	Content string `json:"content"`
	Ord     int    `json:"ord"`
}

func (BookChapter) TableName() string {
	return "book_chapter"
}

// BookInfo
//
//	@Description: 关联表名
//	@Author: zarttic
type BookInfo struct {
	Id         int     `json:"id"`
	Author     string  `json:"author"`
	Name       string  `json:"name"`
	Tags       string  `json:"tags"`
	Img        string  `json:"img"`
	Intro      string  `json:"intro"`
	Kind       string  `json:"kind"`
	Words      float64 `json:"words"`
	ChapterNum int     `json:"chapter_num"`
	ClickCount int     `json:"click_count"`
}

// TableName
// ////////////////////////////////
//
//	@Description:
//	@receiver BookInfo
//	@return string
//
// ////////////////////////////////
func (BookInfo) TableName() string {
	return "book_info"
}
