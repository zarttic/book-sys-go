/**
 *@filename       M5.go
 *@Description
 *@author          liyajun
 *@create          2022-12-22 12:33:37
 */

package utils

import (
	"crypto/md5"
	"encoding/hex"
)

// M5Encode
// ////////////////////////////////
//
//	@Description: md5加密
//	@param str
//	@return string
//
// ////////////////////////////////
func M5Encode(str string) string {
	hash := md5.New()
	hash.Write([]byte(str))
	return hex.EncodeToString(hash.Sum(nil))
}
