package models

import "time"

// TODO
//
//	@Description:
//	@Author: zarttic
type TODO struct {
	Id         int       `json:"id"`
	Title      string    `json:"title"`
	Status     bool      `json:"status"`
	UpdateTime time.Time `json:"update_time"`
}

// TableName
// ////////////////////////////////
//
//	@Description: 关联表名
//	@receiver TODO
//	@return string
//
// ////////////////////////////////
func (TODO) TableName() string {
	return "todo"
}
