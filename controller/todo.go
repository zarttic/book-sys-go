package controller

import (
	"book/dao"
	"fmt"
	"github.com/gin-gonic/gin"
)

// AddToList
// ////////////////////////////////
//
//	@Description: 添加到list里面
//	@param ctx
//
// ////////////////////////////////
func AddToList(ctx *gin.Context) {

	val := struct {
		Title string `json:"title"`
	}{}
	_ = ctx.BindJSON(&val)
	fmt.Println(val.Title)
	cnt := dao.AddToList(val.Title)
	if cnt < 1 {
		ctx.JSON(200, gin.H{
			"msg":  "添加失败",
			"code": "201",
		})
		return
	}
	ctx.JSON(200, gin.H{
		"msg":  "添加成功",
		"code": "200",
	})
}

var (
	cnt = 0
)

// SetToDo
// ////////////////////////////////
//
//	@Description: 修改状态
//	@param ctx
//
// ////////////////////////////////
func SetToDo(ctx *gin.Context) {
	//val := struct {
	//	Id int `json:"id"`
	//}{}
	//err := ctx.BindJSON(&val)
	//if err != nil {
	//	fmt.Println(err)
	//	return
	//}
	//value := val.Id
	cnt += 1
	fmt.Println(cnt)
	if cnt%2 == 0 {
		ctx.JSON(200, gin.H{
			"msg":  "成功",
			"code": "200",
		})
		if cnt == 10 {
			cnt = 0
		}
		return
	}
	value := ctx.Query("id")
	cnt := dao.Set(value)
	//time.Sleep(3 * time.Second)
	if cnt < 1 {
		ctx.JSON(200, gin.H{
			"msg":  "操作失败，请重试！",
			"code": 201,
		})
		return
	}
	ctx.JSON(200, gin.H{
		"msg":  "成功",
		"code": "200",
	})

}

// Find
// ////////////////////////////////
//
//	@Description: 查询
//	@param ctx
//
// ////////////////////////////////
func Find(ctx *gin.Context) {
	todo := dao.Find()
	//idx := 10
	//if idx > len(todo) {
	//	idx = len(todo)
	//}
	//todo = todo[:idx]
	ctx.JSON(200, gin.H{
		"data": todo,
		"code": 200,
		"msg":  "查询成功",
	})
}

// DelTodo
// ////////////////////////////////
//
//	@Description: 删除待办事项
//	@param ctx
//
// ////////////////////////////////
func DelTodo(ctx *gin.Context) {
	val := struct {
		Id int `json:"id"`
	}{}
	ctx.BindJSON(&val)
	dao.DelTodo(val.Id)
	ctx.JSON(200, nil)
}
