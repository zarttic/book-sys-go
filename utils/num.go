package utils

import (
	"fmt"
	"strconv"
)

// Decimal
// ////////////////////////////////
//
//	@Description: 数据处理
//	@param value
//	@return float64
//
// ////////////////////////////////
func Decimal(value float64) float64 {
	value, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", value), 64)
	return value
}
