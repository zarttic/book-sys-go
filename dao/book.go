package dao

import (
	"book/models"
	"book/utils"
)

// BookNums
// ////////////////////////////////
//
//	@Description: 统计所有的书
//	@return count
//
// ////////////////////////////////
func BookNums() (count int64) {

	GDB.Model(&models.BookInfo{}).Count(&count)
	return
}

// ChapterNums
// ////////////////////////////////
//
//	@Description: 统计所有的章数
//	@return count
//
// ////////////////////////////////
func ChapterNums() (count int64) {
	GDB.Model(&models.BookChapter{}).Count(&count)
	return
}

// BookPage
// ////////////////////////////////
//
//	@Description: 图书分页查询
//	@param no
//	@param size
//	@return books
//	@return cnt
//
// ////////////////////////////////
func BookPage(no, size int) (books []models.BookInfo, cnt int64) {
	GDB.Scopes(utils.Paginate(no, size)).Find(&books).Count(&cnt)
	return
}

// BookStatic
// ////////////////////////////////
//
//	@Description: 统计小说的种类 以及占比
//	@return info
//	@return cnt
//
// ////////////////////////////////
func BookStatic() (info []models.BookInfo, cnt int64) {
	GDB.Find(&info).Count(&cnt)
	return
}
func ChangeIntroByID(id, intro string) {
	var tem models.BookInfo
	GDB.Find(&tem).Where("id = ?", id)
	tem.Intro = intro
	GDB.Save(&tem)
}
