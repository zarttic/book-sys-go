package routers

import (
	"book/controller"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

// InitRouter
// ////////////////////////////////
//
//	@Description: 路由
//	@return *gin.Engine
//
// ////////////////////////////////
func InitRouter() *gin.Engine {
	r := gin.Default()
	r.Use(cors.Default())
	//r.Use(mid.CORSMiddleware())
	verGP := r.Group("/api/v1")
	{

		//使用下面的count代替 已弃用
		verGP.GET("/usercount", controller.UserCount)
		verGP.GET("/bookcount", controller.BookNum)
		verGP.GET("/chaptercount", controller.ChapterNum)
		//获取count的统一接口
		verGP.GET("/count", controller.Count)
		verGP.GET("/static", controller.BookStatic)

		userApi := verGP.Group("/user")
		{
			userApi.GET("/pages", controller.UserPages)
			userApi.GET("/changename", controller.ChangeUserName)
			userApi.DELETE("/del", controller.DelUser)
			userApi.POST("/add", controller.RegisterUser)
		}
		bookApi := verGP.Group("/book")
		{
			bookApi.GET("/pages", controller.BookPages)
			bookApi.GET("/all", controller.BookAll)
			bookApi.GET("/rank", controller.ReadRank)
			bookApi.POST("/intro", controller.ChangeIntro)
		}
		adminApi := verGP.Group("/admin")
		{
			adminApi.POST("/login", controller.Login)
			adminApi.POST("/register", controller.Register)
			adminApi.POST("/change", controller.ChangePassword)
		}

		todoApi := verGP.Group("/todo")
		{
			todoApi.POST("/add", controller.AddToList)
			todoApi.GET("/set", controller.SetToDo)
			todoApi.GET("/get", controller.Find)
			todoApi.POST("/del", controller.DelTodo)
		}

	}
	return r
}
