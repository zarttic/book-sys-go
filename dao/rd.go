package dao

import (
	"github.com/gomodule/redigo/redis"
)

var GRD redis.Conn

// InitRD
// ////////////////////////////////
//
//	@Description: 初始化redis数据库
//
// ////////////////////////////////
func InitRD() {
	GRD, _ = redis.Dial("tcp", "127.0.0.1:6379")
}
