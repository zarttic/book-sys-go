package dao

import (
	"book/models"
	"time"
)

// AddToList
// ////////////////////////////////
//
//	@Description: 添加到todo里面
//	@param str
//	@return cnt
//
// ////////////////////////////////
func AddToList(str string) (cnt int64) {
	GDB.Create(&models.TODO{
		Title:      str,
		Status:     false,
		UpdateTime: time.Now(),
	}).Count(&cnt)
	return
}

// Set
// ////////////////////////////////
//
//	@Description: 修改todo的状态
//	@param id
//	@return cnt
//
// ////////////////////////////////
func Set(id string) (cnt int64) {
	cur := models.TODO{}
	GDB.Where("id = ?", id).Find(&cur)
	cur.Status = !cur.Status
	cur.UpdateTime = time.Now()
	GDB.Save(cur).Count(&cnt)
	return
}

// Find
// ////////////////////////////////
//
//	@Description: 查询所有的todo
//	@return todo
//
// ////////////////////////////////
func Find() (todo []models.TODO) {
	GDB.Find(&todo)
	return
}

// DelTodo
// ////////////////////////////////
//
//	@Description: 删除todo
//	@param id
//
// ////////////////////////////////
func DelTodo(id int) {
	GDB.Delete(&models.TODO{}, id)
}
