package utils

import (
	"fmt"
	"gorm.io/gorm"
)

// Paginate
// ////////////////////////////////
//
//	@Description: 分页查询插件
//	@param page
//	@param pageSize
//	@return func(db *gorm.DB) *gorm.DB
//
// ////////////////////////////////
func Paginate(page, pageSize int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if page == 0 {
			page = 1
		}

		fmt.Println(pageSize)
		switch {
		case pageSize > 100:
			pageSize = 100
		case pageSize <= 0:
			pageSize = 10
		}

		offset := (page - 1) * pageSize
		return db.Offset(offset).Limit(pageSize)
	}
}
